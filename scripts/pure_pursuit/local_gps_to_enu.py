import numpy as np

GPS_TO_METERS = 111319.9  # each degree is that many meters on the equator

def gps_to_xy(lat, lon):
    return np.array([lon*GPS_TO_METERS*np.cos(lat*np.pi/180), lat*GPS_TO_METERS])
    # return np.array([lat*GPS_TO_METERS, -lon*GPS_TO_METERS*np.cos(lat*np.pi/180)])